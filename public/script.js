function localhost(){
  return location.hostname === "localhost" || location.hostname === "127.0.0.1" || location.hostname === ""
}
var radio_config = {
  ip :(localhost() ? "http" : "https" ) + "://radio.takk-r.com",
  playlist_link : "/public/takkr/playlist.pls",
  radio_link : "/radio/8000/radio.mp3",
  json_link : "/api/nowplaying_static/takkr.json",
  socket_link: "/api/live/nowplaying/takkr"
}

const MarkdownIt = window.markdownit({html: true, linkify:true});

function url_tag(){
  let r = window.location.href.split("#")
  return r.length > 1 ? r[1] : null
}

function navigate(link){
  window.location.href = link
}

var player = null
var showList = function(){}
var toggleMenu = function(){}
var sheet = null 
var chat_visible = false

var volume_snap = 0.075


function tabTitle(status){
  if(status.live){
    return status.streamer + " - " + status.title
  }else{
    return (status.artist == "" ? "" : status.artist + " - ") + status.title + " @ Takk-R"
  }
}
function linkify(t){
  t.find("a").attr("target", "_blank")
  return t;
}

function render(t){
  return MarkdownIt.render(t.replace(/\n/g, "\ \ \n"))
}
function practicallyMobile(){
  return isMobile.any || (window.innerHeight > window.innerWidth || window.innerWidth < 640);
}
function resize(){
  if(isMobile.any || practicallyMobile()){
    $("#mobile_style").prop("disabled", false)
    // if(chat_visible){
    //   toggleChat()
    // }
  }else{
    $("#mobile_style").prop("disabled", true)

    if(!chat_visible){
      toggleChat()
    }
    // $("#title-scroll").css({
    //   width : $("#titlebar").width() - $("#title-button").width()
    // }
    // )
  }

  $("#track").css({
    position : "fixed",
    width : $("#sidebar").height(),
    height : $("#sidebar").width(),
    top : $("#sidebar").position().top - $("#sidebar").width(),
    left : $("#sidebar").position().left,
  }).show()

  fitChatframe()

}

function scrolledText(text, characters, offset){
  if(text == "")
    return ""
  else{
    let o = offset % Math.max(characters, text.length)
    let t = ""
    while(t.length < Math.max(characters, text.length) * 2)
      t += text  + " "
    return t.substring(o, o + characters)
  }
}

let scroll_frame = 0
let scroll_tick = window.setInterval(scrolls, 300)
let current_scroll = 0

function scrollDiv(selector, frame, text){
  $(selector).html(scrolledText(
    text,
    $(selector).width() / 15,
    frame
  ))
}

function scrolls(){
  scroll_frame++
  if(player){
    // document.title = scrolledText(player.status.text, 24, scroll_frame)
    scrollDiv("#track",scroll_frame, player.getText())
  }

  if(sheet){
    // if(scroll_frame % 10 == 0 && Math.random() < 0.2){
    //   renderEmpty(sheet)
    // }else{
      $(".scrolling").map(function(i){
        if(Math.random() > 0.5){
          let s = $(".scrolling").get(i)
          let f = $(s).data("frame")
          $(s).data("frame", f + 1)
          scrollDiv(s, f, $(s).data("text"))
        }
      })
    // }
  }
}

function onPlayerUpdate(status){
  $("#live-button").toggleClass("live", status.live)
  $("#play").toggleClass("playing", status.playing)
  $("#listener-count").html(status.listeners) // + " hallgató"
  document.title = tabTitle(status)
}

function onStreamerChange(streamer){
  if(sheet && sheet.loaded){
    if(player.status.live){
      if(!player.status.playing){
        player.toggle({forced : true})
      }
      window.location.hash = sheet.getUsername(player.status.streamer)
    }else{
      window.location.hash = ""
      // renderEmpty(sheet)
    }
  }
}

function toggleChat(){
  chat_visible = !chat_visible

  $("#chat").toggle(chat_visible)
  $("#chat-button").toggleClass("opened", chat_visible)
  $("#listeners, #listener-count, #listener-button").toggleClass("opened", chat_visible)
  fitChatframe()
}
function fitChatframe(){
  $("#chat-frame")
    .attr({width : $("#chat").width(), height : $("#chat").height()})
    .css({width : $("#chat").width(), height : $("#chat").height()})

  if(chat_visible){
    $("#chat-frame").get(0).contentWindow.postMessage("scrollChat","*");
    // $("#chat-frame").scrollTop(100000)
  }
}

function revealChat(){
  $("#chat-frame").show()
}

function createChat(){
  let src = "https://minnit.chat/takkrradio?embed&mobile&nickname="

  if(window.location.href.indexOf("?chat") != -1){
    src = "https://takk-r-chat.glitch.me"
    window.addEventListener("message", function(event) {
      if(event.data == "ready")
        revealChat()
    });
  }else{
    window.setTimeout(revealChat, 5000)
  }
  let chat = $('<iframe>', {
    id : "chat-frame",
    src : src,
    style : "border:none;",
    loaded : function(){
    }
  })
  .attr({width : 0, height : 0})
  .css({width : 0, height : 0})
  .appendTo("#chat")
  .hide()

  
}


function liveIcon(){
  return $("<div>", {id : "live-icon", html : "live"})
}

function newsPost(post){
  return $("<div>", {
    class : "post",
    html : render(post.news)
  })
}

function streamerPage(streamer, live){
  return [
    $("<div>", {
      id : "streamer",
      html : [streamer.DisplayName].concat(live ? [liveIcon()] : [])
    }),
    $("<div>", {
      id : "about",
      html : render(streamer.About)
    })
  ]
}

function randomScrolls(_sheet){
  let scrolls = []
  let l = 1 + randomId(3)
  for(let i = 0; i<l; i++){
    let s = randomFrom(_sheet.Scroll)
    scrolls.push($("<div>", {
        id : "scroll", 
        class : "scrolling"
      }).data({
        text : s.ScrollText,
        frame : 0
      })
    )
  }
  return scrolls;
}

function streamersList(streamers, status){
  return streamers.sort((a, b) => (a.DisplayName.toLowerCase() > b.DisplayName.toLowerCase()) ? 1 : -1).map(function(s){
    let live = status.live && status.streamer == s.DisplayName;
    return $("<div>", {
      class : "streamer-link", 
      html : s.DisplayName
    }).click(function(){
      window.location.hash = s.Username
    }).on("mouseenter", function(e){
      $(this).css("background-color", randomColor())
    }).on("mouseleave", function(e){
      $(this).css("background-color", "")
    }).append(live ? liveIcon() : "")
    }
  )
}

function renderStreamer(name){
  let streamer = sheet.getStreamer(name)
  if(streamer)
    renderPage(streamerPage(streamer, player.live(streamer.DisplayName)), streamer.Image, streamer.Background)
}

function renderEmpty(_sheet){
  let image = randomFrom(_sheet.Images).Link
  // renderPage(randomScrolls(_sheet), image)
  // console.log(_sheet.News[0])
  // renderPage(_sheet.News.map(newsPost), image)
  renderPage(newsPost(_sheet.News[0]), image)
}

function renderPage(content, image_url, background_size){
  if(!background_size)
    background_size = ""
  $("#page")
    .html(content)
    .css({
      backgroundImage : "url(" + image_url + ")",
      backgroundSize : background_size,
    }) 
}
function randomId(l){
  return Math.floor(Math.random() * l)
}
function randomFrom(array){
  return array[randomId(array.length)]
}

function showStreamer(_sheet, status){
  let tag = url_tag()
  let name = tag ? tag : (status ? (status.streamer != "" ? status.streamer : null) : null)
  if(name){
    renderStreamer(name)
  }else{
    renderEmpty(_sheet)
  }
}
function clamp(x, min, max){
  return Math.min(max, Math.max(min, x))
}

function randomColor(){
  return "hsl(" + Math.random() * 360 + ", 80%, 70%)"
}
function hideLoading(){
  $("#loading").hide()
}


function sheetReady(_sheet){
  console.log(_sheet)
  showList = function(){
    $("#page").html(streamersList(_sheet.Streamers, player.status))
  }
  toggleMenu = function(){
    if(practicallyMobile() && chat_visible)
      toggleChat()
    if($("#page").find(".streamer-link").length == 0){
      showList()
    }else
      showStreamer(_sheet, player.status)
  }

  $("#menu-button").click(toggleMenu).show()
  window.onhashchange = () => showStreamer(sheet, player.status)

  if(window.location.hash == ""){
    if(player.status.live)
      window.location.hash = _sheet.getUsername(player.status.streamer)
    else{
      renderEmpty(_sheet)
      // toggleMenu()
    }
  }else{
    renderStreamer(_sheet.getStreamer(window.location.hash.substring(1)).DisplayName)
  }
  hideLoading()
  // showStreamer(_sheet, player.status)
}

function volumeToColor(x){
  return "hsl(" + (10 + x * 60) + ", "+(80 * (1-x))+"%, 40%"
}
function pickVolume(e){
  var x = e.originalEvent.offsetY / $("#sidebar").height();
  var snap = volume_snap
  if(x < snap)
    x = 0
  else if( x > 1 - snap)
    x = 1
  player.setVolume(x)
  scroll_frame += Math.floor(Math.random() * 200)
  $("#volume").css({
    height :(clamp(x, snap, 1) * 100) + "%",
    backgroundColor : volumeToColor(x)
  })
}

function createVolume(){
  if(!isMobile.any){
    $("#sidebar").click(pickVolume).mousemove(function (e){
      if(e.buttons != 0)
        pickVolume(e)
    }).on("mouseenter", function(e){
      $("#volume").css("background-color", volumeToColor(player.radio.volume))
    }).on("mouseleave", function(e){
      if(player.radio.volume > volume_snap)
        $("#volume").css("background-color", "")
    })
  }else{
    $("#volume").remove()
  }
}

function init(status){
  createChat()
  $("#chat-button").click(toggleChat)
  resize()
  $(window).on("resize", resize)
  createVolume()
  $("#titlebar").click(function(){navigate("/")})

  sheet = new Sheet(sheetReady)
}

player = new Player({
  config:radio_config
  ,radio : $("#radio").get(0)
  ,play : $("#play")
  ,m3u : $("#m3u")
  ,update : onPlayerUpdate
  ,onStreamerChange : onStreamerChange
  ,onReady : init
})
