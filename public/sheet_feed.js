class Sheet{
  constructor(x){
    this.ready_callback = x;
    this.getUsername = function(displayName){
      let r = this.Streamers.find(
        (s) => s.DisplayName == displayName
      )
      return r ? r.Username : ""
    }    
    this.getStreamer = function(n){
      let r = this.Streamers.find(
        (s) => s.DisplayName == n || s.Username == n
      )
      return r
    }

    this.init = function(json) {
      console.log(json)
      this.loaded = true
      for(let k in json){
        this[k] = json[k]
      }
      this.ready_callback(this)
    }

    $.getJSON("sheet.json", this.init.bind(this))

  }
}
