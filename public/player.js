class Player {
  constructor(x) {
    this.radio = x.radio;
    this.ip = x.config.ip;
    this.playlist_link = x.config.playlist_link;
    this.radio_link = x.config.radio_link;
    this.json_link = x.config.json_link;
    this.socket_link = x.config.socket_link;
    this.play_button = x.play;
    this.m3u_button = x.m3u;
    this.artist = x.artist;
    this.title = x.title;
    this.ready = x.onReady;
    this.stop_sign = x.stop_sign;
    this.play_sign = x.play_sign;
    this.update_callback = x.update;
    this.streamer_switch_callback = x.onStreamerChange;
    this.first_fetch = true;
    this.status = {};

    this.setVolume = function(x){
      this.radio.volume = x;
    }
    // this.status_link = function () {
    //   return this.ip + this.json_link;
    // }

    this.subscribe_link = function(){
      return this.ip + this.socket_link;
    }
    this.m3u_link = function(){ return this.ip + this.playlist_link }    
    this.src_link = function(){ return this.ip + this.radio_link }

    this.toggle = function(options){
      if(options.forced != undefined){
        if(options.forced)
          this.radio.play()
        else
          this.radio.pause()
      }else{
        if(this.radio.paused)
          this.radio.play()
        else
          this.radio.pause()
      }
      this.status.playing = !this.radio.paused
      this.update_callback(this.status)
    }    
    this.parse = function(data){
      // console.log(data)
      return {
        playing : !this.radio.paused
        ,live : data.live.is_live
        ,streamer : data.live.streamer_name
        ,artist : data.now_playing.song.artist 
        ,title : data.now_playing.song.title
        ,text : data.now_playing.song.text
        ,listeners : data.listeners.current
      }
    }
    this.status_changed = function(new_status){
      var keys = Object.keys(this.status)
      for(var k of keys){
        if(this.status[k] != new_status[k]){
          // console.log("change",k, this.status[k],new_status[k])
          return true;
        }
      }
      return false;
    }
    this.getText = function(){
      let t = ""
      if(this.status.artist != ""){
        t+= this.status.artist
      }
      if(this.status.title != ""){
        if(t != "")
          t += " - ";
        t += this.status.title
      }
      if(t == "")
        t = this.status.streamer
      return t + " ";
    }
    this.changeStatus = function(s){
      let t = s.title
      if(t == "" || t == "Unknown" || s.text == "Unknown"){
        t = this.status.title
      }
      this.status = s
      this.status.title = t
    }
    this.update = function(s, forced){
      if(this.status_changed(s) || forced || this.first_fetch){
        if(this.status.live != s.live || this.status.streamer != s.streamer){
          this.status.title = ""
          this.changeStatus(s)
          this.streamer_switch_callback(s)
        }else{
          this.changeStatus(s)
        }
        if(this.first_fetch){
          this.ready(this.status)
          this.first_fetch = false
        }
        this.update_callback(this.status)
      }
    }
    this.get_message = function (data) {
      this.update(this.parse(JSON.parse(data)))
    }
    this.init_subscribe = function(){
      this.sub = new NchanSubscriber(this.subscribe_link());
      this.sub.on("message", this.get_message.bind(this));
      this.sub.start();
    };
    this.live = function(displayName){
      return this.status.live && this.status.streamer == displayName;
    }

    this.download_m3u = function() {
      window.location.href = this.m3u_link();
      // window.location.href = this.playlist_link;
    }
    this.radio.src = this.src_link()
    this.play_button.click(this.toggle.bind(this))
    this.m3u_button.click(this.download_m3u.bind(this))
    this.init_subscribe()
  }
}
