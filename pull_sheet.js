import fetch from "node-fetch"
import * as fs from "fs"
import * as papa from "papaparse"
const config = {
  ID : "2PACX-1vRQqpa6qT-EvFM6nhi83jTCZHc2JVzm-bsq3Zd3VSm6f90qFOfOV-w_gLx1hRS-YQ-MBMja-xf96fXi",
  GIDS : {
    Streamers : "961592627",
    Images : "604790356",
    Scroll : "1661918613",
    News : "0" 
  }
}

async function parseCSV(text){
  let rows = (await papa.default.parse(text)).data
  let props = rows[0]
  return rows.slice(1).map((r, i) => {
    let s = {}
    props.map((p, pi) =>{
      s[p] = r[pi]
    })
    return s
  })
}

let fetch_tab = async function(gid, callback){
  console.log("fetching ", gid)
  let url = 'https://docs.google.com/spreadsheets/d/e/'+ config.ID +'/pub?output=csv&gid=' + config.GIDS[gid]
  let response = await fetch(url)
  let text = await response.text()
  return text
}

let init = async function() {
  let sheet = {
    Streamers : await parseCSV(await fetch_tab("Streamers")),
    Images : await parseCSV(await fetch_tab("Images")),
    Scroll : await parseCSV(await fetch_tab("Scroll")),
    News : await parseCSV(await fetch_tab("News")),
  }
  console.log(sheet)
  fs.writeFileSync("./public/sheet.json", JSON.stringify(sheet))
}

// console.log(papa.default.parse)

init()

// export init
// module.exports = init